CREATE DATABASE prova;

USE prova;

CREATE TABLE usuario(
  login varchar(20) primary key,
  senha varchar(200) not null
);

CREATE TABLE cliente(
  cpf char(11) not null primary key,
  nome varchar(50) not null,
  email varchar(100) not null, 
  sexo char(1) not null,
  estado char(2) not null
);

INSERT INTO cliente VALUES('08708708721', 'Ramon', 'ramon.artner@gmail.com', 'M', 'SC');
INSERT INTO cliente VALUES('09855944381', 'Elisa', 'elisa.lisa@gmail.com', 'F', 'SC');
INSERT INTO cliente VALUES('49083527638', 'Gean', 'gboss@gmail.com', 'M', 'SC');
INSERT INTO cliente VALUES('53512270611', 'Brayan', 'couringa.cs3gta3pw3.jokers@gmail.com', 'M', 'SC');
INSERT INTO cliente VALUES('66332121866', 'Camila', 'mila66@gmail.com', 'F', 'SC');
